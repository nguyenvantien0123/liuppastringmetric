package liuppa;

public class ExperimentOutput implements Comparable{
	private String metric;
	private double precision;
	private double maxF1;
	
	public ExperimentOutput(String metric, double precision, double maxF1) {
		super();
		this.metric = metric;
		this.precision = precision;
		this.maxF1 = maxF1;
	}
	public double getPrecision() {
		return precision;
	}
	public void setPrecision(double precision) {
		this.precision = precision;
	}
	public double getMaxF1() {
		return maxF1;
	}
	public void setMaxF1(double maxF1) {
		this.maxF1 = maxF1;
	}
	public String getMetric() {
		return metric;
	}
	public void setMetric(String metric) {
		this.metric = metric;
	}
	@Override
	public int compareTo(Object o2) {
		// TODO Auto-generated method stub
		double precision2 = ((ExperimentOutput)o2).getPrecision();
		if(this.precision>precision2)return -1;
		else if (this.precision==precision2)return 0;
		else return 1;
	}
	
	public String toString(){
		return this.metric + ", " + this.precision + ", " + this.maxF1;
	}
}
