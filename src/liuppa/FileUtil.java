/************************************************************************************************************************************************
 *project: annotation n-ary relation
 *@author NGUYEN Van Tien nguyenvantien0123@yahoo.com
 *@date May 9, 2008
 *@copyright EDELWEISS project, INRIA Sophia Antipolis, France 
 *2004 route des Lucioles - BP 93 FR-06902 SOPHIA-ANTIPOLIS Cedex, FRANCE
 *************************************************************************************************************************************************/
package liuppa;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


/************************************************************************************************************************************************
 *Type name: FileUtil
 *@author NGUYEN Van Tien 
 *@date May 9, 2008
 *Description: for reading and writing the file
 *************************************************************************************************************************************************/
public class FileUtil {
	private String fileName;
	private File file;
	private FileWriter fileWriter;
	private FileReader fileReader;
	
	public boolean isEmptyFile(){
		if(this.getFileLength() == 0)return true;
		return false;
	}
	public long getFileLength(){
		return this.file.length();
	}
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public FileUtil(String fileName) {
		this.fileName = fileName;
		file = new File(fileName);		
	}
	/**
	 * ******************************************************************************************************************************************
	 * @param s 
	 * return void
	 * Description: write string s to the file
	 ********************************************************************************************************************************************
	 */
	public void write(String s){
		try {
			fileWriter = new FileWriter(file);
			fileWriter.write(s);
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			System.out.println("Exception when write to file " + fileName);
			e.printStackTrace();
		}
	}
	
	public String readFile(){
		String content = new String();	
		try {
			this.fileReader = new FileReader(file);		
			
			long fileLength = this.file.length();
			
			char[] charBuffer = new char[(new Long(fileLength)).intValue()];			
			
			int readedCharNumber = this.fileReader.read(charBuffer);
			if (readedCharNumber != -1){
				content = new String(charBuffer);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}  catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			if (fileReader != null) {
				try {
					fileReader.close();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}	
		}
		return content;
	}
	
	
	
	/**
	 * 
	 * @param path : path to the directory
	 * @return : list of file in this directory
	 */
	public static ArrayList<String> getFilesInDir(String path){
		ArrayList<String> files = new ArrayList();
		File dir = new File(path);
		File fileList [] = dir.listFiles();
		for(int i = 0 ; i < fileList.length ; i++){
			//			String url =  path + "\\" + fileList[i].getName(); //Window
			String url =  path + "/" + fileList[i].getName(); //linux
			files.add(url);
		}
		return files;
	}
	
	public static String getDirOfFile(String file){
		int k = file.lastIndexOf(new String("/"));
		String dir = file.substring(0, k);
		return dir;				
	}
	
	public static String getFileName(String file){
		int k = file.lastIndexOf(new String("/"));
		String name = file.substring(k + 1, file.length());
		return name;
	}
	
	public static String getExtension(String file){
		int k = file.lastIndexOf(".");
		if(k<0)return "";
		String extension = file.substring(k, file.length());
		return extension;
	}
	
	public static String getFileNameWithoutExtension(String file){
		String nameWithExtension = getFileName(file);
		int k = nameWithExtension.lastIndexOf(".");
		return nameWithExtension.substring(0, k);
		
	}
	public static String readFile(String fileName){
		FileUtil reader = new FileUtil(fileName);
		return reader.readFile();
	}
	
	public static void writeFile(String fileName, String content){
		FileUtil writer = new FileUtil(fileName);
		writer.write(content);
	}
}
