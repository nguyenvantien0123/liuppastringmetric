package liuppa;

import liuppa.charactermetric.NeedlemanWunch;
import liuppa.charactermetric.Qgram;

import com.wcohen.ss.JaroWinkler;
import com.wcohen.ss.MongeElkan;
import com.wcohen.ss.NeedlemanWunsch;
import com.wcohen.ss.SoftTFIDF;
import com.wcohen.ss.TFIDF;
import com.wcohen.ss.TagLink;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s1 = "bureau de poste";
		String s2 = "poste de radio";
		
		JaroWinkler j =new JaroWinkler();
		System.out.println(j.score("de", "des"));
		
		Qgram q =new Qgram();
		System.out.println(q.score("fer", "ferrée"));
		
		TagLink metric = new TagLink();
		
		double score = metric.score(s1, s2);
		System.out.println(score);
		
		LiuppaMetric m2 = new LiuppaMetric(Constant.JARO_WINKLER_ALGORITHM, Constant.QGRAMDISTANCE_ALGORITHM);
//		double score2 = m2.score(s1, s2); 
//		System.out.println("s1s2  "+score2);
		System.out.println(s2 + "," + s1);
		double score2 = m2.score(s1, s2); 
		System.out.println("s1s2   "+score2);
//		test2();	
		
		TFIDF m3 = new TFIDF();
		double score3 = m3.score(s1, s2);
		System.out.println(score3);
		
		SoftTFIDF m4 = new SoftTFIDF(new JaroWinkler());
		double score4 = m3.score(s1, s2);
		System.out.println(score4);
		
		MongeElkan el  = new MongeElkan();
		System.out.println(el.score("1234", "534"));
		System.out.println(el.score("abc", "debc"));
		System.out.println(el.score("abcd", "ecd"));
	}
	public static void test2(){
		String []s={"hotel", "de", "pr�fecture", "h�tel", "de", "d�partement"};
		NeedlemanWunch metric = new NeedlemanWunch();
		for(String s1 : s){
			for(String s2 :s){
				System.out.println(s1 + "," + s2 + "=" + metric.score(s1, s2));
			}
		}
	}
	
	public void test1(){
		// TODO Auto-generated method stub
//		String file = "E:\\projects\\StringMetricEvaluation\\Output\\keywordLabel\\tmpData.txt";
		String file = "E:\\projects\\StringMetricEvaluation\\Output\\keywordLabel\\tmpData2.txt";
//		String s1 = "TokenBl   liuppa.       avg";
		
		String s1 = "       avg";
		String s = FileUtil.readFile(file);
//		System.out.println(s);
		int[] metrics = {9, 17, 23, 33, 39};
		int i = 0;
		while(s.contains(s1)){
			i++;
			s = s.replaceFirst(s1, "  Avg for metric" + i);
		}
		s = s.replaceAll("  ", ":");
		System.out.println(s);
	}

}
