package liuppa;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import liuppa.charactermetric.I_Sub;
import liuppa.charactermetric.Levenshtein;
import liuppa.charactermetric.NeedlemanWunch;
import liuppa.charactermetric.Qgram;
import liuppa.charactermetric.SmithWaterman;
import liuppa.charactermetric.TFIDFChar;

import com.wcohen.ss.AbstractStringDistance;
import com.wcohen.ss.Jaccard;
import com.wcohen.ss.Jaro;
import com.wcohen.ss.JaroWinkler;
import com.wcohen.ss.JaroWinklerTFIDF;
import com.wcohen.ss.Level2Jaro;
import com.wcohen.ss.Level2JaroWinkler;
import com.wcohen.ss.Level2Levenstein;
import com.wcohen.ss.Level2MongeElkan;
import com.wcohen.ss.MongeElkan;
import com.wcohen.ss.TFIDF;
import com.wcohen.ss.TagLink;

public class Experimentation {
	static ArrayList<Pair> allPairs = new ArrayList<Pair>();
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Processing, write output to file output.txt");
		PrintStream standard = System.out;
		setOut("output.txt");
		
		// TODO Auto-generated method stub
		testForArticle2();
//		testForArticle1();
		System.setOut(standard);
		System.out.println("Finish, see file output.txt");

	}
	
	public static void experiment(){
		String file1="E:\\projects\\StringMetricEvaluation\\data\\Tien\\Christian_correctPairs.txt";
		String file2 = "E:\\projects\\StringMetricEvaluation\\data\\Tien\\Christian_incorrectPairs.txt";
		ArrayList<Pair> correctPairs = getPairs(file1, true);
		ArrayList<Pair> incorrectPairs = getPairs(file2, false);
		
		allPairs.addAll(correctPairs);
		allPairs.addAll(incorrectPairs);
		ArrayList<AbstractStringDistance> metrics = initAllMetrics();
		ArrayList<ExperimentOutput> output = new ArrayList<ExperimentOutput>();
				
		for(AbstractStringDistance m : metrics){
			ExperimentOutput o = evaluatesMetric(m, allPairs);
			output.add(o);
		}
		Collections.sort(output);
		int i = 0;
		System.out.println("-------------------The best metrics following precision---------------------");
		for(ExperimentOutput out:output){
			i++;
			System.out.println(i + "."+ out);
		}

//		for(Pair pair:allPairs){
//			System.out.println(pair);
//		}
	}
	
	static ArrayList<AbstractStringDistance> initAllMetrics(){
		ArrayList<AbstractStringDistance> metrics = new ArrayList<AbstractStringDistance>();
		//liuppa metrics
		for(int i = 1; i <=9 ; i++) 
			for(int j = 1 ; j <= 9 ; j++){
				LiuppaMetric metric = new LiuppaMetric(i, j);
				metric.setDescription("Liuppa(" + i + ", "+j + ")");
				metrics.add(metric);
			}
		//other metrics
		metrics.add(new Jaro());
		metrics.add(new JaroWinkler());
		metrics.add(new Levenshtein());
		metrics.add(new NeedlemanWunch());
		metrics.add(new SmithWaterman());
		metrics.add(new MongeElkan());
		metrics.add(new Qgram());
		metrics.add(new MongeElkan());
		metrics.add(new liuppa.charactermetric.Jaccard());
		metrics.add(new TFIDFChar());
		metrics.add(new I_Sub());
		//token based metrics
		metrics.add(new Jaccard());
		metrics.add(new TFIDF());
		//hybrid metrics
		metrics.add(new Level2Jaro());
		metrics.add(new Level2JaroWinkler());
		metrics.add(new Level2Levenstein());
		metrics.add(new Level2MongeElkan());
		metrics.add(new JaroWinklerTFIDF());
		
		metrics.add(new TagLink());
		
		for(int i = 0  ; i< metrics.size() ; i++){
			AbstractStringDistance metric = metrics.get(i);
			String description = metric.getDescription();
			if(description==""||description == null)description = metric.getClass().getName();
			metric.setDescription(description);
			metrics.set(i, metric);
		}
		return metrics;
	}
	
	/*
	 * evaluate a metric thank to a list of pairs
	 */
	static ExperimentOutput evaluatesMetric(AbstractStringDistance metric, ArrayList<Pair> pairs){
		//get score for each pair
		calcultateScore(pairs, metric);
		//sort the pairs
		Collections.sort(pairs);
		//calculate precision
		double precision = precision(pairs);
		String description = metric.getDescription();
		if(description==""||description == null)description = metric.getClass().getName();
		ExperimentOutput output = new ExperimentOutput(description, precision, 0.0);
		return output;
	}
	
	static double precision(ArrayList<Pair> pairs){
		double n = 0;
		double sumPrecision = 0;
		int numCorrectPairs = 0;
		
		for (int i=0; i<pairs.size(); i++) {
			Pair pair = pairs.get(i);
			if (pair.isCorrect()) {
				n++;
				double precisionAtRankI = n/(i+1.0);
				sumPrecision += precisionAtRankI;
				numCorrectPairs++;
			}
		}
		return new Double(sumPrecision / numCorrectPairs);
	}
	
	static void calcultateScore(ArrayList<Pair> pairs, AbstractStringDistance metric){
		for(int i = 0 ; i < pairs.size() ; i++){
			Pair pair = pairs.get(i);
			pair.calculateScore(metric);
			pairs.set(i, pair);			
		}
		System.out.println("------------evaluating metric " + metric.getDescription()+ "----------------------");
		for(Pair pair:allPairs){
			System.out.println(pair);
		}
	}
	
	/*
	 * -get pair in file, pair may be correct or incorrect
	 * 
	 */
	public static ArrayList<Pair> getPairs(String file, boolean isCorrect){
		ArrayList<Pair> pairs = new ArrayList<Pair> ();
		String s = FileUtil.readFile(file);
		s = s.trim();
		if(s==""||s==null)return null;
		String[]lines = s.split("\n");
		for(String line : lines){
			line = line.trim();
			if(line!=""&&line!=null){
				String[] x = line.split(",");
				if(x.length<2)continue;
				String s1 = x[0].trim();
				String s2 = x[1].trim();
				Pair pair = new Pair(s1, s2);
				pair.setCorrect(isCorrect);
				pairs.add(pair);
			}
		}
		return pairs;		
	}
	/**
	 * 
	 * @param file
	 * set standard output to file
	 */
	public static void setOut(String file){
		try {
			System.setOut(new PrintStream(new FileOutputStream(file)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void testForArticle1(){
//		String file = "E:\\documents_de_these\\Conferences\\Mesure de la similarité de concepts ontologiques\\paire.txt";
		String file = "pairForArticle.txt";
		String file1="E:\\projects\\StringMetricEvaluation\\data\\Tien\\Christian_correctPairs.txt";
		String file2 = "E:\\projects\\StringMetricEvaluation\\data\\Tien\\Christian_incorrectPairs.txt";
//		file=file1;
//		file=file2;
		ArrayList<Pair> pairs = getPairs(file, true);


		ArrayList<AbstractStringDistance> metrics = new ArrayList<AbstractStringDistance> ();
		metrics.add(new JaroWinkler());
		metrics.add(new Levenshtein());
		metrics.add(new Jaccard());
		metrics.add(new TFIDF());
		metrics.add(new JaroWinklerTFIDF());
		metrics.add(new TagLink());
		metrics.add(new LiuppaMetric(1,1));
		for(int i = 0  ; i< metrics.size() ; i++){
			AbstractStringDistance metric = metrics.get(i);
			String description = metric.getDescription();
			if(description==""||description == null)description = metric.getClass().getSimpleName();
			metric.setDescription(description);
			metrics.set(i, metric);
		}
//		for(AbstractStringDistance metric : metrics){
//			System.out.println("------------evaluating metric " + metric.getDescription()+ "----------------------");
//			calcultateScore(pairs, metric);
//			for(Pair pair:pairs){
//				System.out.println(pair);
//			}
//		}
		
		for(Pair pair:pairs){
			String s1 = pair.getS1();
			String s2 = pair.getS2();
			String s = s1 + ","+s2 + "\n";
			for(AbstractStringDistance metric : metrics){
				double score = pair.calculateScore(metric);				
				String metricName = metric.getDescription();
				s += metricName+"="+score + ";" + "\n";
			}
			System.out.println(s);
		}
	}
		
	public static void testForArticle2(){
			System.out.println("Processing, write output to file output.txt");
			PrintStream standard = System.out;
			setOut("output.txt");
			
			experiment();
			
			
			System.setOut(standard);
			System.out.println("Finish, see file output.txt");
		}
	
	
}
