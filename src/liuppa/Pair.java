package liuppa;

import java.util.Comparator;

import com.wcohen.ss.AbstractStringDistance;

public class Pair implements Comparable{
	private String s1;
	private String s2;
	private boolean isCorrect;
	private double score;
	
	public String getS1() {
		return s1;
	}

	public void setS1(String s1) {
		this.s1 = s1;
	}

	public String getS2() {
		return s2;
	}

	public void setS2(String s2) {
		this.s2 = s2;
	}

	public double calculateScore(AbstractStringDistance metric){
		double score = metric.score(s1, s2);
		this.score = score;
		return score;
	}
	
	public String toString(){
		return "(" + s1 + ", " + s2 + ", " + isCorrect + ", " + score + ")";
	}
	public Pair(String s1, String s2) {
		super();
		this.s1 = s1;
		this.s2 = s2;
	}
	
	
	@Override
	public int compareTo(Object pair2) {
		// TODO Auto-generated method stub		
		double score1 = this.getScore();
		double score2 = ((Pair)pair2).getScore();
		if(score1>score2)return -1;
		else if(score1==score2)return 0;
		else return 1; //score1 < score2				
	}
	public boolean isCorrect() {
		return isCorrect;
	}
	public void setCorrect(boolean isCorrect) {
		this.isCorrect = isCorrect;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	
	
}
