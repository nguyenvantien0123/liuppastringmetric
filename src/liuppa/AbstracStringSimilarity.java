package liuppa;

public abstract class AbstracStringSimilarity {
	abstract public double getSimilarity(String s1,String s2);
}
