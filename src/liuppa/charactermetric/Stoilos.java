package liuppa.charactermetric;

import java.util.ArrayList;

import com.wcohen.ss.AbstractStringDistance;
import com.wcohen.ss.JaroWinkler;
import com.wcohen.ss.api.StringWrapper;

public class Stoilos extends AbstractStringDistance{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
			
		
		// TODO Auto-generated method stub
		String s1 = "nguyenvantien"; 
		String s2 = "thihiennguyen";
//		s1="A";s2="A";
//		s1="Allée (carrossable)";s2="Allée (forestière)";
	
		JaroWinkler winklerMetric = new JaroWinkler();

		System.out.println("JaroWinkler:" + winklerMetric.score(s1, s2));
	
		Stoilos metric = new Stoilos();
//		System.out.println(metric.longestCommonSubstring2(s1, s2));
		System.out.println(metric.commonSubstrings(s1, s2));
		System.out.println("Sloilos:" + metric.score(s1, s2));

	}

	@Override
	public String explainScore(StringWrapper s, StringWrapper t) {
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public double score(StringWrapper s, StringWrapper t) {
		// TODO Auto-generated method stub
		String s1 = s.unwrap();
		String s2 = t.unwrap();
		int l1 = s1.length();
		int l2 = s2.length();
		/*
		 * calculate the similarity between s1, s2 by the metric proposed by Stoilos (extension of JaroWinkler the the paper
		 * "A String Metric for Ontology Alignment"
		 */
		String u1 = new String(s1);//initial unmatched string for s1
		String u2 = new String(s2);//initial unmatched string for s2
		ArrayList<String> commonSubstrings = commonSubstrings(s1, s2);
//		System.out.println("common substrings = " + commonSubstrings);
		int lc = 0 ; 
		for(String substring : commonSubstrings){
			lc+= substring.length();
			u1 = removeFirstSubstring(u1, substring);//final u1 is obtained by removing all common substring from s1
			u2 = removeFirstSubstring(u2, substring);
			
		}
		//commm(s1, s2)
		double comm = (double)2*lc/(l1 + l2);
//		System.out.println("comm=" + comm);
		double p = 0.6;		//recommended by the author of this metric
//		System.out.println("u1:" + u1);
//		System.out.println("u2:" + u2);
		double ul1 = (double)u1.length()/l1;
		double ul2 = (double)u2.length()/l2;
//		System.out.println("ul1:" + ul1);
//		System.out.println("ul2:" + ul2);
		//diff(s1,s2)
		double diff = (double)(ul1*ul2)/(p + (1-p)*(ul1 + ul2-ul1*ul2));
//		System.out.println("diff="+diff);
		JaroWinkler winklerMetric = new JaroWinkler();
		double jaroWinkler = winklerMetric.score(s, t);
		double stoilosScore = comm-diff+jaroWinkler; 
//		System.out.println("JaroWinkler="+jaroWinkler);
//		System.out.println("stoilos ="+stoilosScore);
		return stoilosScore;
	}
	/*
	 * search common susstring of s1, s2
	 * for example : s1 = "nguyenvantien"; s2="thihiennguyen";
	 * return :{"nguyen",  "ien", "t"}
	 */
	ArrayList<String> commonSubstrings(String s1, String s2){		
		ArrayList<String> results = new ArrayList<String>();
//		System.out.println(s1 + "-" + s2);
		int n = 0;
		while(true){
		
			String longestCommonSubstring = longestCommonSubstring1(s1, s2);
			if(longestCommonSubstring!=null&&longestCommonSubstring.length()!=0){
				results.add(longestCommonSubstring);
//				System.out.println(s1 + "-" + s2 + "-"+longestCommonSubstring);
//				s1 = s1.replaceFirst(longestCommonSubstring, "");
//				s2 = s2.replaceFirst(longestCommonSubstring, "");
				s1 = removeFirstSubstring(s1,longestCommonSubstring);
				s2 = removeFirstSubstring(s2,longestCommonSubstring);
				
				if(s1.trim()==""||s2.trim()=="")break;
			}else {
				break;
			}
		}
		return results;
	}

	/*
	 * search the longest common sub string betweend two string
	 * for exemple : the first = "nguyenvantien", the second = "thihiennguyen";
	 * return : nguyen
	 */
	public  String longestCommonSubstring1(String first, String second) {		 
		 String tmp = "";
		 String max = "";
						
		 for (int i=0; i < first.length(); i++){
			 for (int j=0; j < second.length(); j++){
				 for (int k=1; (k+i) <= first.length() && (k+j) <= second.length(); k++){
										 
					 if (first.substring(i, k+i).equals(second.substring(j, k+j))){
						 tmp = first.substring(i,k+i);
					 }
					 else{
						 if (tmp.length() > max.length())
							 max=tmp;
						 tmp="";
					 }
				 }
					 if (tmp.length() > max.length())
							 max=tmp;
					 tmp="";
			 }
		 }				
		 return max;        			  
	}

//	function LCSubstr(S[1..m], T[1..n])
//    L := array(1..m, 1..n)
//    z := 0
//    ret := {}
//    for i := 1..m
//        for j := 1..n
//            if S[i] = T[j]
//                if i = 1 or j = 1
//                    L[i,j] := 1
//                else
//                    L[i,j] := L[i-1,j-1] + 1
//                if L[i,j] > z
//                    z := L[i,j]
//                    ret := {}
//                if L[i,j] = z
//                    ret := ret ∪ {S[i-z+1..i]}
//    return ret
	
	/*
	 * the best
	 */
	public  String longestCommonSubstring2(String S, String T){
		int m = S.length();
		int n = T.length();
		int[][]L = new int[S.length()][T.length()];
		int z = 0;
		String ret = new String("");
		for (int i = 0 ; i < m ;i++){
			for(int j = 0 ; j < n ; j++){
				if(S.charAt(i)==T.charAt(j)){
					if(i==0||j==0){
						L[i][j]=1;
					}else{
						L[i][j] = L[i-1][j-1]+1;
					}
				
					if(L[i][j]>z){
						z=L[i][j];
						ret=new String("");
					}
					
					if(L[i][j]==z){
						ret = S.substring(i-z+1, i+1);						
					}
				}
			}
		}
		return ret;
	}
	
	
	public static String longestCommonSubstring3(String first, String second) {
		int fl = first.length();
		int sl = second.length();
		int[][] table = new int[fl + 1][sl + 1];
		for (int i = 0; i <= fl; i++)
			table[i][0] = 0;
		for (int i = 0; i <= sl; i++)
			table[0][i] = 0;
		for (int i = 1; i < fl + 1; i++) {
			for (int j = 1; j < sl + 1; j++) {
				if (first.charAt(i - 1) == second.charAt(j - 1)) {
					table[i][j] = table[i - 1][j - 1] + 1;
				} else {
					if (table[i - 1][j] > table[i][j - 1])
						table[i][j] = table[i - 1][j];
					else
						table[i][j] = table[i][j - 1];
				}
			}
		}
		for(int i = 0 ; i <= fl ; i++){
			for(int j = 0 ; j <sl ; j++){
				System.out.print(table[i][j] + " ");
			}
			System.out.println();
		}
		return null;
	}

	public  String removeFirstSubstring(String s, String subString){
		String r = new String();
		int i = s.indexOf(subString);
		r = s.substring(0, i) + s.substring(i+subString.length());
		return r;
	}
	
}
