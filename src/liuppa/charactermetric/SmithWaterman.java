package liuppa.charactermetric;

import com.wcohen.ss.AbstractStringDistance;
import com.wcohen.ss.api.StringWrapper;

public class SmithWaterman extends AbstractStringDistance {

	@Override
	public String explainScore(StringWrapper s, StringWrapper t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double score(StringWrapper s, StringWrapper t) {
		// TODO Auto-generated method stub
		String s1 = s.unwrap();
		String s2 = t.unwrap();		
		//use NeedlemanWunch metric of Sam
		uk.ac.shef.wit.simmetrics.similaritymetrics.SmithWaterman smithWaterman = new uk.ac.shef.wit.simmetrics.similaritymetrics.SmithWaterman();
		float score = smithWaterman.getSimilarity(s1, s2);
		return (double)score;
	}

}
