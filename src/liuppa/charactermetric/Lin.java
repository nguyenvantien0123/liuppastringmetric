package liuppa.charactermetric;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.wcohen.ss.AbstractStringDistance;
import com.wcohen.ss.api.StringWrapper;
/*
 * Implement the string metric trigram proposed by Lin in the paper 
 * "An Information-Theoretic Definition of Similarity"
 */
public class Lin extends AbstractStringDistance{
	int window=3;//trigram
	public static void main(String args[]){
		String s1 = "université";
		String s2 = "university";
		Lin metric = new Lin();
		System.out.println(metric.score(s1, s2));
	}
	@Override
	public String explainScore(StringWrapper s, StringWrapper t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double score(StringWrapper s, StringWrapper t) {
		// TODO Auto-generated method stub
		String s1 = s.unwrap();
		String s2 = t.unwrap();
		
		ArrayList<String> q1 = buildQgrams(s1);
//		System.out.println(q1);
		ArrayList<String> q2 = buildQgrams(s2);
//		System.out.println(q2);
		
		ArrayList<String> allQgrams = new ArrayList<String>();
		allQgrams.addAll(q1);
		allQgrams.addAll(q2);
		
		HashSet<String> commonQgrams = getCommonQgrams(q1, q2);		
//		System.out.println(commonQgrams);
		//probabilities of all qgrams in 2 string
		HashMap<String, Double> probabilities = getQgramsProbability(allQgrams);
		double x = 0;
		double y = 0;
		//calculate x
		for(String qgram : commonQgrams){
			double p = probabilities.get(qgram);//probability of qgram
			x+=Math.log10(p);
		}
		
		//calculate y
		HashMap<String, Double> probabilities1 = getQgramsProbability(q1);
		for(String qgram:q1){
			double p = probabilities1.get(qgram);
			y+=Math.log10(p);
		}
		HashMap<String, Double> probabilities2 = getQgramsProbability(q2);
		for(String qgram:q2){
			double p = probabilities.get(qgram);
			y+=Math.log10(p);
		}
		double score = 2*x/y;
//		System.out.println(probabilities);
//		System.out.println(probabilities1);
//		System.out.println(probabilities2);
		return score;
	}
	
	ArrayList<String> buildQgrams(String s){
		ArrayList<String> qgrams = new ArrayList<String>();
		for(int i = 0 ; (i+window) <= s.length();i++){
			String qgram = s.substring(i, i+window);
			qgrams.add(qgram);
		}
		return qgrams;
	}
	
	HashSet<String> getCommonQgrams(ArrayList<String> q1, ArrayList<String> q2){
		return intersection(q1, q2);
	}
	
	HashSet<String> intersection(ArrayList<String> l1, ArrayList<String> l2){
		HashSet<String> set = new HashSet<String>();
		for(String s : l1){
			if(l2.contains(s)){
				set.add(s);
			}
		}
		return set;
	}
	
	/*
	 * probabilty of qgram in the list
	 */
	double getProbability(String qgram, ArrayList<String> qgramList){
		double d =0 ;
		int count=0;
		for(String s : qgramList){
			if(qgramList.contains(s))count++;
		}
		return qgramList.size()>0 ? count/qgramList.size() : 0;
	}
	
	/*
	 * calculate the probability of qgram element in the qgram list
	 */
	HashMap<String, Double> getQgramsProbability(ArrayList<String> allQgrams){
		
		HashMap<String, Double> map = new HashMap<String, Double>();//pair (qgram, probability)
		ArrayList<String> qgramsSet = new ArrayList(new HashSet(allQgrams));
		int all = allQgrams.size();
		for(String qgram : qgramsSet){
			int count = 0;
			for(String s : allQgrams){
				if(qgram.equalsIgnoreCase(s))count++;
			}
			Double p = (double)count/all;
			map.put(qgram, p);
		}
		return map;
	}
}
