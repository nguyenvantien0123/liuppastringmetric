package liuppa.charactermetric;

import com.wcohen.ss.AbstractStringDistance;
import com.wcohen.ss.TFIDF;
import com.wcohen.ss.api.StringWrapper;
/*
 * version of TFIDF for character
 */
public class TFIDFChar extends AbstractStringDistance{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TFIDFChar metric = new TFIDFChar();
		System.out.println(metric.score("abc", "cba"));
		System.out.println(metric.score("NGUYEN Van Tien", "Tien Nguyen Van"));
	}


	@Override
	public String explainScore(StringWrapper s, StringWrapper t) {
		return null;
	}

	@Override
	public double score(StringWrapper s, StringWrapper t) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		//ex : s = abc, t = bcf (a, b, c, f represente the tokens)
		String s1 = s.unwrap();
		String s2 = t.unwrap();
		s1=convert(s1);
		s2=convert(s2);
		TFIDF metric = new TFIDF();
		return metric.score(s1, s2);
	}
	
	private String convert(String s1){
		//ex : s1 = "abc", s2 = "a b c";
		String s2 = new String();
		for(int i = 0 ; i < s1.length() ;i++){
			s2 += s1.charAt(i) + " ";
		}	
		return s2;
	}

}
