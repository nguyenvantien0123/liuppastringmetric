package liuppa.charactermetric;

import com.wcohen.ss.AbstractStringDistance;
import com.wcohen.ss.api.StringWrapper;

public class Qgram extends AbstractStringDistance {

	@Override
	public String explainScore(StringWrapper s, StringWrapper t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double score(StringWrapper s, StringWrapper t) {
		// TODO Auto-generated method stub
		String s1 = s.unwrap();
		String s2 = t.unwrap();		
		//use NeedlemanWunch metric of Sam
		uk.ac.shef.wit.simmetrics.similaritymetrics.QGramsDistance qGramsDistance = new uk.ac.shef.wit.simmetrics.similaritymetrics.QGramsDistance();
		float score = qGramsDistance.getSimilarity(s1, s2);
		return (double)score;
	}
	
	public static void main(String args[]){
		Qgram l= new Qgram();
		System.out.println(l.score("nguyen van tien", "van tien nguyen"));
	}

}
