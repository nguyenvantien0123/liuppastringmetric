package liuppa.charactermetric;

import com.wcohen.ss.AbstractStringDistance;
import com.wcohen.ss.api.StringWrapper;

public class NeedlemanWunch extends AbstractStringDistance{

	@Override
	public String explainScore(StringWrapper s, StringWrapper t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double score(StringWrapper s, StringWrapper t) {
		// TODO Auto-generated method stub
		String s1 = s.unwrap();
		String s2 = t.unwrap();		
		//use NeedlemanWunch metric of Sam
		uk.ac.shef.wit.simmetrics.similaritymetrics.NeedlemanWunch needlemanWunch = new uk.ac.shef.wit.simmetrics.similaritymetrics.NeedlemanWunch();
		float score = needlemanWunch.getSimilarity(s1, s2);
		return (double)score;
	}

}
