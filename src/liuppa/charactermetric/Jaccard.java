/**
 * PhD Project 2009 - 2011, by NGUYEN Van Tien
 * Université de Pau et des pays de l'Adour
 * GEONTO project
 * This file is created on 10 déc. 2010
 */
package liuppa.charactermetric;

import java.util.HashSet;
import java.util.Set;

import com.wcohen.ss.AbstractStringDistance;
import com.wcohen.ss.api.StringWrapper;

public class Jaccard extends AbstractStringDistance {

	/* (non-Javadoc)
	 * @see com.wcohen.ss.AbstractStringDistance#score(com.wcohen.ss.api.StringWrapper, com.wcohen.ss.api.StringWrapper)
	 */
	@Override
	public double score(StringWrapper s, StringWrapper t) {
		//created on 10 déc. 2010
		String s1 = s.unwrap();
		String s2 = t.unwrap();
		double common = 0;
		Set<Character> set1 =this.getSetCharacter(s1);
		Set<Character> set2 =this.getSetCharacter(s2);
		
		
		for(Character c : set1){
			if(set2.contains(c))common++;
		}
		
		return common/(s1.length()+s2.length()-common);
	}
	
	private Set<Character> getSetCharacter(String s){
		Set<Character> set = new HashSet<Character>();
		for(int i = 0 ; i < s.length();i++){
			Character c = s.charAt(i);
			set.add(c);
		}		
		return set;
	}

	/* (non-Javadoc)
	 * @see com.wcohen.ss.AbstractStringDistance#explainScore(com.wcohen.ss.api.StringWrapper, com.wcohen.ss.api.StringWrapper)
	 */
	@Override
	public String explainScore(StringWrapper s, StringWrapper t) {
		//created on 10 déc. 2010
		
		return null;
	}

}
