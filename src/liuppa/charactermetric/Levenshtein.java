package liuppa.charactermetric;

import com.wcohen.ss.AbstractStringDistance;
import com.wcohen.ss.api.StringWrapper;

public class Levenshtein extends AbstractStringDistance{

	@Override
	public String explainScore(StringWrapper s, StringWrapper t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double score(StringWrapper s, StringWrapper t) {
		// TODO Auto-generated method stub
		String s1 = s.unwrap();
		String s2 = t.unwrap();		
		uk.ac.shef.wit.simmetrics.similaritymetrics.Levenshtein metric = new uk.ac.shef.wit.simmetrics.similaritymetrics.Levenshtein();
		float score = metric.getSimilarity(s1, s2);
		return (double)score;
	}
	
	public static void main(String args[]){
		Levenshtein l= new Levenshtein();
		System.out.println(l.score("Pau", "Paris"));
		
		System.out.println(l.score("ABCD", "DEFEA"));

	}


}
