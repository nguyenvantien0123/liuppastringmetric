/**
 * PhD Project 2009 - 2011, by NGUYEN Van Tien
 * Université de Pau et des pays de l'Adour
 * GEONTO project
 * This file is created on 7 déc. 2010
 */
package liuppa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


import com.wcohen.ss.AbstractStringDistance;
import com.wcohen.ss.Jaro;
import com.wcohen.ss.JaroWinkler;
import com.wcohen.ss.Levenstein;
import com.wcohen.ss.MongeElkan;
import com.wcohen.ss.NeedlemanWunsch;
import com.wcohen.ss.TagLink;
import com.wcohen.ss.api.StringWrapper;
import com.wcohen.ss.api.Token;
import com.wcohen.ss.tokens.SimpleTokenizer;



public class LiuppaMetric extends AbstractStringDistance{
	//the first metric used by string convertor
	private int metric1; 
	AbstractStringDistance firtMetric;
	//the secode metric used to compare sequence of special letter
	private int metric2;
	AbstractStringDistance secondMetric;
	
	private ArrayList<Character> alphabetic; //the special letter which will replace the tokens in the string
	private double threshold;//threshold to considering two token are identical
	private Set<String> C = new HashSet<String>();//set of tokens converted
	private Map<String, Character> P = new HashMap<String, Character>();//list of pair (token, special letter which replace token)
	private int k = 0;//the current letter of alphabetic which is used to replace the tokens
	/**
	 *---------------------------------------------------------------------------------------------------------
	 * @param args
	 */
	public static void main(String[] args) {
		
		
//		metric.script();
//		genString();
		test();

	}
	public static void genString(){
		int JARO_WINKLER_ALGORITHM = 1;
		int LEVENSHTEIN_ALGORITHM = 2;
		int NEEDLEMAN_WUNCH_ALGORITHM = 3;
		int SMITH_WATERMAN_ALGORITHM = 4;
		int QGRAMDISTANCE_ALGORITHM = 5;
		int MONGE_ELKAN_ALGORITHM = 6;
		int JARO_ALGORITHM = 7;
		int JACCARD_CHAR_ALGORITHM=8;//version character of Jaccard
		String[] metrics = {"JaroWinkler",
							"Levenshtein",
							"NeedlemanWunch",
							"SmithWaterMan",
							"Qgram",
							"MongeElkan",
							"Jaro",
							"Jaccard"};
		int n = 0 ;
		for(int i = 0 ; i < metrics.length;i++){
			for(int j = 0 ; j < metrics.length;j++){
				n++;
				System.out.println(n + ": Liuppa-" + metrics[i]+"-"+metrics[j]);
			}
			
		}
		
		
		String basicMetrics[]={"Jaro", 
		"JaroWinkler_I",
		 "Levenstein_I",
		 "SmithWaterman_I",
		 "NeedlemanWunsch_I",
		 "MongeElkan_I",
		 "Qgram_I",
		 "TFIDF_II",
		"Jaccard_II",
		"TagLink_III",
		"SoftTFIDF_III",
		"Level2Jaro_III",
		"Level2JaroWinkler_III",
		"Level2Levenstein_III",
		"Level2MongeElkan_III"
		};
		char c = 'A';
		for(String s : basicMetrics){
			System.out.println(c + ":" + s );
			c++;
		}
		
	}
	
	public static void test(){
//		LiuppaMetric metric = new LiuppaMetric(Constant.NEEDLEMAN_WUNCH_ALGORITHM, Constant.JARO_WINKLER_ALGORITHM);
		//created on 7 déc. 2010
		LiuppaMetric metric = new LiuppaMetric(Constant.JARO_WINKLER_ALGORITHM, Constant.LEVENSHTEIN_ALGORITHM);

		System.out.println(metric.getAlphabetic());
//		double score = metric.score("eau traité par usine", "usine de traitement des eaux");
//		double score = metric.score("hotel de pr�fecture", "h�tel de d�partement");
//		double score = metric.score("hotel de pr�fecture", "h�tel de d�partement");
		double score1 = metric.score("nguyenVanTien", "nguyen-van-tien");
		double score2 = metric.score("chemin de pierre", "pierre");
		double score3 = metric.score("eau traitée dans une usine", "usine de traitement des eaux ");

//		System.out.println(score);
		System.out.println(score1);
		System.out.println(score2);
		System.out.println(score3);
		
//		TagLink tagLink = new TagLink();
//		score = tagLink.score("Mauro Gaio", "GAIO Mauro");

//		System.out.println(score);
	}
	
	public static void script(){
		for(int i = 1 ; i <=8 ; i++){
			for(int j = 1 ; j <=8;j++){
				String s = "distance liuppa.LiuppaMetric[metric1=" + i+",metric2="+j+"]";
				System.out.println(s);
			}
		}
	}
	
	public double getSimilarity(String s1, String s2){
//		System.out.println(s1);
//		System.out.println(s2);
		ArrayList<String> S1 = this.tokenisation(s1);
		ArrayList<String> S2 = this.tokenisation(s2);
		if(S1.size()==1&&S2.size()==1){
			return this.firtMetric.score(s1, s2);
		}
//		System.out.println(S1);
//		System.out.println(S2);

		/*
		 * for each time we want to calculate sim(s1, s2), we must initialize 
		 * 		-C(set of token converted), 
		 * 		-P (pair (token, special letter))
		 *      -k is initiated to the first caracter of alphabetic
		 */
		C.clear();
		P.clear();
		k = 0;
		//init C, P by the first token of s1
		String firstToken = S1.get(0);//the first token of S1
		Character firstLetter = this.alphabetic.get(0); //the first letter in the alphabetic
		C.add(firstToken);//add the first token of S1 to C
		P.put(firstToken, firstLetter);
		//convert S1 to string of special character
		s1 = convert(S1); 
		//convert S2 to string of special character
		s2 = convert(S2);		
//		System.out.println(s1);
//		System.out.println(s2);
		//use the seconde metric to calculate the final score on the converted string
		double score = secondMetric.score(s1, s2);
//		System.out.println(score);
		return score;
	}
	
	
	@Override
	public double score(StringWrapper s, StringWrapper t){
		String s1 = s.unwrap();
		String s2 = t.unwrap();			
		return this.getSimilarity(s1, s2);
	}
	
	/**
	 *---------------------------------------------------------------------------------------------------------
	 * @param metric22
	 * @return
	 */
	private AbstractStringDistance getMetricClass(int m) {
		//created on 7 déc. 2010
		AbstractStringDistance metric;		  
		  switch(m){
			case Constant.JARO_WINKLER_ALGORITHM :
				JaroWinkler jaroWinkler = new JaroWinkler();
				metric = jaroWinkler;
				break;			
			case Constant.LEVENSHTEIN_ALGORITHM:			
				//use metric implemented by Sam
				liuppa.charactermetric.Levenshtein levenstein = new liuppa.charactermetric.Levenshtein();
				metric = levenstein;
				break;
			case Constant.NEEDLEMAN_WUNCH_ALGORITHM:
				//use metric implemented by Sam
				liuppa.charactermetric.NeedlemanWunch needlemanWunsch = new liuppa.charactermetric.NeedlemanWunch();
				metric = needlemanWunsch;
				break;
			case Constant.SMITH_WATERMAN_ALGORITHM:				
				//use metric implemented by Sam
				liuppa.charactermetric.SmithWaterman smithWaterman = new liuppa.charactermetric.SmithWaterman();
				metric = smithWaterman;
				break;		
			case Constant.QGRAMDISTANCE_ALGORITHM:				
				//use metric implemented by Sam
				liuppa.charactermetric.Qgram qgram = new liuppa.charactermetric.Qgram();
				metric = qgram;
				break;		
			
			case Constant.MONGE_ELKAN_ALGORITHM :
				MongeElkan mongelkan = new MongeElkan();
				metric = mongelkan;
				break;
				
			case Constant.JARO_ALGORITHM:
				Jaro jaro = new Jaro();
				metric = jaro;
				break;
			
			case Constant.JACCARD_CHAR_ALGORITHM:
				liuppa.charactermetric.Jaccard jaccard = new liuppa.charactermetric.Jaccard();
				metric = jaccard;
				break;
				
			case Constant.I_SUB:
				liuppa.charactermetric.I_Sub isub = new liuppa.charactermetric.I_Sub();
				metric = isub;
			default :
				jaroWinkler = new JaroWinkler();
				metric = jaroWinkler;
				break;
		  }
		return metric;
	}

	/*
	 * convert a each token in sequence of token S by a special letter 
	 */
	private String  convert(ArrayList<String> S){
		String output = new String();
		for(int i = 0 ; i < S.size() ; i++){
			String Si = S.get(i);//token i of S
			//search the token the most similar to token Si
			Map<String, Double> pair = maxSim(Si, this.C);
			Set<String> tmp = pair.keySet();
			Object[] tokens = tmp.toArray();
			
			//String[] tokens = (String[])pair.keySet().toArray();
			String simToken = (String)tokens[0];//the token the most similar : tokens array contains only one element
			Double score = pair.get(simToken);//the score between Si and simToken
			
			if(score >= this.threshold){//Si and simToken are considered identical
				Character alpha = this.P.get(simToken);//the letter which represented the simToken
				//alpha become the representation of Si
				output += alpha;
				this.P.put(Si, alpha);//mark that Si is reprensented by alpha
			}else{//nothing among the token converted is similar to Si
				k++;
				Character beta = this.alphabetic.get(k);//the next letter in alphabetic
				output += beta;//beta become the representation of Si
				this.P.put(Si, beta);
			}
			
			this.C.add(Si);//mark that Si is converted

		}

		return output;
	}
	
	
	/**
	 *---------------------------------------------------------------------------------------------------------
	 * @param token
	 * @param tokenSet
	 * @return the token the most similar to s among the token in the set of token, and the score
	 */
	private Map<String, Double> maxSim(String token, Set<String> tokenSet) {
		//created on 7 déc. 2010
		Double maxScore = -1d;
		String simToken = token;//the token the most similar
		for(String t : tokenSet){
			Double score = this.firtMetric.score(token, t);
			if(score > maxScore){
				maxScore = score;
				simToken = t;
			}
		}
		
		Map<String, Double> pair = new HashMap<String, Double>();
		pair.put(simToken, maxScore);
		return pair;
	}

	/*
	 * default contructor  
	 */
	public LiuppaMetric(){
		this(Constant.JARO_WINKLER_ALGORITHM, Constant.JARO_WINKLER_ALGORITHM);
	}
	
	/**
	 * @param metric1
	 * @param metric2
	 */
	public LiuppaMetric(int metric1, int metric2) {
		this(metric1, metric2, 1);
		initDefaultThreshold();
	}

	public LiuppaMetric(int metric1, int metric2, float threshold){
		super();
		//set the code of metrics
		this.metric1 = metric1;
		this.metric2 = metric2;
		//get the metrics object by theirs code
		this.firtMetric = this.getMetricClass(metric1);
		this.secondMetric = this.getMetricClass(metric2);
		initAlphabetic();
		this.threshold = threshold;
	}

	/*
	 * init alphabetic : each letter in it will replace a token of a string
	 */
	private void initAlphabetic(){
		this.alphabetic = new ArrayList<Character>();
//		for(int i = 945 ; i < 10000 ; i++){
		for(int i = 65 ; i < 10000 ; i++){
			Character c = (char) i;
			this.alphabetic.add(c);
//			System.out.println(i + " = " + c);
		}
	}
	/**
	 *---------------------------------------------------------------------------------------------------------
	 */
	private void initDefaultThreshold() {
		switch(this.metric1){
		case Constant.JARO_WINKLER_ALGORITHM :
			this.threshold = 0.84d;
			break;
		case Constant.LEVENSHTEIN_ALGORITHM:
			this.threshold = 0.79d;
			break;
			
		case Constant.NEEDLEMAN_WUNCH_ALGORITHM:
			this.threshold = 0.88d;
			break;
		case Constant.SMITH_WATERMAN_ALGORITHM:
			this.threshold = 0.83d;
			break;
		case Constant.QGRAMDISTANCE_ALGORITHM:
			this.threshold = 0.60d;
			break;
		case Constant.MONGE_ELKAN_ALGORITHM:
			this.threshold = 0.84;
			break;
		case Constant.JARO_ALGORITHM :
			this.threshold = 0.8d;
			break;
			
		default :
			this.threshold = 0.8d;
			break;
		}
	}

	/**
	 * @return the metric1
	 */
	public int getMetric1() {
		return metric1;
	}



	/**
	 * @return the metric2
	 */
	public int getMetric2() {
		return metric2;
	}



	/**
	 * @return the alphabetic
	 */
	public ArrayList<Character> getAlphabetic() {
		return alphabetic;
	}



	/**
	 * @param metric1 the metric1 to set
	 */	
	public void setMetric1(Integer metric1) {
		this.metric1 = metric1;
		this.firtMetric = this.getMetricClass(metric1);
		this.initDefaultThreshold();
	}



	/**
	 * @param metric2 the metric2 to set
	 */
	public void setMetric2(Integer metric2) {		
		this.metric2 = metric2;
		this.secondMetric = this.getMetricClass(metric2);		
	}



	public double getThreshold() {
		return threshold;
	}


	public void setThreshold(Double threshold) {
		this.threshold = threshold;
	}


	/**
	 * @param alphabetic the alphabetic to set
	 */
	public void setAlphabetic(ArrayList<Character> alphabetic) {
		this.alphabetic = alphabetic;
	}



	/*
	 * 
	 */
	private ArrayList<String> tokenisation(String s){
		ArrayList<String> l = new ArrayList<String>();
		
		
		SimpleTokenizer tokenizer = SimpleTokenizer.DEFAULT_TOKENIZER;
		Token[] tokens = tokenizer.tokenize(s);
		for(Token t: tokens){
			l.add(t.getValue());
		}
		return l;
	}

	@Override
	public String explainScore(StringWrapper s, StringWrapper t) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
