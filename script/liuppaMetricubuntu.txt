blocker TokenBlocker

distance liuppa.LiuppaMetric[metric1=1,metric2=1]
distance liuppa.LiuppaMetric[metric1=1,metric2=2]
distance liuppa.LiuppaMetric[metric1=1,metric2=3]
distance liuppa.LiuppaMetric[metric1=1,metric2=4]
distance liuppa.LiuppaMetric[metric1=1,metric2=5]
distance liuppa.LiuppaMetric[metric1=1,metric2=6]
distance liuppa.LiuppaMetric[metric1=1,metric2=7]
distance liuppa.LiuppaMetric[metric1=1,metric2=8]
distance liuppa.LiuppaMetric[metric1=2,metric2=1]
distance liuppa.LiuppaMetric[metric1=2,metric2=2]
distance liuppa.LiuppaMetric[metric1=2,metric2=3]
distance liuppa.LiuppaMetric[metric1=2,metric2=4]
distance liuppa.LiuppaMetric[metric1=2,metric2=5]
distance liuppa.LiuppaMetric[metric1=2,metric2=6]
distance liuppa.LiuppaMetric[metric1=2,metric2=7]
distance liuppa.LiuppaMetric[metric1=2,metric2=8]
distance liuppa.LiuppaMetric[metric1=3,metric2=1]
distance liuppa.LiuppaMetric[metric1=3,metric2=2]
distance liuppa.LiuppaMetric[metric1=3,metric2=3]
distance liuppa.LiuppaMetric[metric1=3,metric2=4]
distance liuppa.LiuppaMetric[metric1=3,metric2=5]
distance liuppa.LiuppaMetric[metric1=3,metric2=6]
distance liuppa.LiuppaMetric[metric1=3,metric2=7]
distance liuppa.LiuppaMetric[metric1=3,metric2=8]
distance liuppa.LiuppaMetric[metric1=4,metric2=1]
distance liuppa.LiuppaMetric[metric1=4,metric2=2]
distance liuppa.LiuppaMetric[metric1=4,metric2=3]
distance liuppa.LiuppaMetric[metric1=4,metric2=4]
distance liuppa.LiuppaMetric[metric1=4,metric2=5]
distance liuppa.LiuppaMetric[metric1=4,metric2=6]
distance liuppa.LiuppaMetric[metric1=4,metric2=7]
distance liuppa.LiuppaMetric[metric1=4,metric2=8]
distance liuppa.LiuppaMetric[metric1=5,metric2=1]
distance liuppa.LiuppaMetric[metric1=5,metric2=2]
distance liuppa.LiuppaMetric[metric1=5,metric2=3]
distance liuppa.LiuppaMetric[metric1=5,metric2=4]
distance liuppa.LiuppaMetric[metric1=5,metric2=5]
distance liuppa.LiuppaMetric[metric1=5,metric2=6]
distance liuppa.LiuppaMetric[metric1=5,metric2=7]
distance liuppa.LiuppaMetric[metric1=5,metric2=8]
distance liuppa.LiuppaMetric[metric1=6,metric2=1]
distance liuppa.LiuppaMetric[metric1=6,metric2=2]
distance liuppa.LiuppaMetric[metric1=6,metric2=3]
distance liuppa.LiuppaMetric[metric1=6,metric2=4]
distance liuppa.LiuppaMetric[metric1=6,metric2=5]
distance liuppa.LiuppaMetric[metric1=6,metric2=6]
distance liuppa.LiuppaMetric[metric1=6,metric2=7]
distance liuppa.LiuppaMetric[metric1=6,metric2=8]
distance liuppa.LiuppaMetric[metric1=7,metric2=1]
distance liuppa.LiuppaMetric[metric1=7,metric2=2]
distance liuppa.LiuppaMetric[metric1=7,metric2=3]
distance liuppa.LiuppaMetric[metric1=7,metric2=4]
distance liuppa.LiuppaMetric[metric1=7,metric2=5]
distance liuppa.LiuppaMetric[metric1=7,metric2=6]
distance liuppa.LiuppaMetric[metric1=7,metric2=7]
distance liuppa.LiuppaMetric[metric1=7,metric2=8]
distance liuppa.LiuppaMetric[metric1=8,metric2=1]
distance liuppa.LiuppaMetric[metric1=8,metric2=2]
distance liuppa.LiuppaMetric[metric1=8,metric2=3]
distance liuppa.LiuppaMetric[metric1=8,metric2=4]
distance liuppa.LiuppaMetric[metric1=8,metric2=5]
distance liuppa.LiuppaMetric[metric1=8,metric2=6]
distance liuppa.LiuppaMetric[metric1=8,metric2=7]
distance liuppa.LiuppaMetric[metric1=8,metric2=8]

 
#
##############################
#data set
dataset /media/PAU/projects/StringMetricEvaluation/data/birdScott1.txt
dataset /media/PAU/projects/StringMetricEvaluation/data/birdKunkel.txt
dataset /media/PAU/projects/StringMetricEvaluation/data/birdNybirdExtracted.txt
dataset /media/PAU/projects/StringMetricEvaluation/data/birdScott2.txt
dataset /media/PAU/projects/StringMetricEvaluation/data/business.txt
dataset /media/PAU/projects/StringMetricEvaluation/data/censusText.txt


 compute
 show blocker
 show learners
 show datasets
 
 precisionRecall
 table maxF1
 table averagePrecision
 table time
 table blockerRecall

 
  
