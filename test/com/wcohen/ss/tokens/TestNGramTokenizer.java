package com.wcohen.ss.tokens;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.wcohen.ss.api.Token;


public class TestNGramTokenizer {

	@Test
	public void testDefaultNGramTokenizer() {
		NGramTokenizer ngt = new NGramTokenizer(3,5,true,SimpleTokenizer.DEFAULT_TOKENIZER);
		
		Token tok[] = ngt.tokenize("testing");
		assertNotNull(tok);
		assertEquals(16, tok.length);
		assertEquals("^testing$", tok[0].getValue());
		assertEquals("^te", tok[1].getValue());
		assertEquals("^tes", tok[2].getValue());
		assertEquals("ing", tok[15].getValue());
	}
}
