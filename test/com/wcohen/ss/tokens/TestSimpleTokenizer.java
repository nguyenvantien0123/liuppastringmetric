package com.wcohen.ss.tokens;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.wcohen.ss.api.Token;


public class TestSimpleTokenizer {

	private String input = "There once was a man from Nantucket\nWho kept all his cash in a bucket.\nBut his daughter, named Nan,\nRan away with a man\nAnd as for the bucket, Nantucket.";
	
	@Test
	public void testSimpleDefaultTokenizer() {
		SimpleTokenizer st = new SimpleTokenizer(true, true);
		
		Token tok[] = st.tokenize(input);
		assertNotNull(tok);
		assertEquals(31, tok.length);
		assertEquals("there", tok[0].getValue());
		
	}
	
	@Test
	public void testNoIgnoreCase() {
		SimpleTokenizer st = new SimpleTokenizer(true, false);
		
		Token tok[] = st.tokenize(input);
		assertNotNull(tok);
		assertEquals(31, tok.length);
		assertEquals("There", tok[0].getValue());		
	}
	
	@Test
	public void testNoIgnorePunctuation() {
		SimpleTokenizer st = new SimpleTokenizer(false, false);
		
		Token tok[] = st.tokenize(input);
		assertNotNull(tok);
		assertEquals(36, tok.length);
		assertEquals(".", tok[15].getValue());
	}
	
	@Test
	public void testAlphanumeric() {
		SimpleTokenizer st = new SimpleTokenizer(false, false);
		
		Token tok[] = st.tokenize("abcd1234");
		assertNotNull(tok);
		assertEquals(2, tok.length);
		assertEquals("abcd", tok[0].getValue());
		assertEquals("1234", tok[1].getValue());
	}
}
