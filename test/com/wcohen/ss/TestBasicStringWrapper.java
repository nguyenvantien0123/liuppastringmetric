package com.wcohen.ss;

import static org.junit.Assert.*;

import org.junit.Test;


public class TestBasicStringWrapper {

	@Test
	public void test() {
		BasicStringWrapper bsw = new BasicStringWrapper("abcd1234");
		assertEquals("abcd1234", bsw.unwrap());
		assertEquals('d', bsw.charAt(3));
		assertEquals(8, bsw.length());
		assertEquals("[wrap 'abcd1234']", bsw.toString());
	}
}
